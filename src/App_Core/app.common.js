﻿
String.prototype.replaceAll = function (target, replacement) {
    return this.split(target).join(replacement);
};

String.prototype.splice = function(idx, rem, str) {
    
    return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};
function setViewportProperty(doc) {
    var customViewportCorrectionVariable = 'vh';
    var prevClientHeight;
    var customVar = '--' + (customViewportCorrectionVariable || 'vh');

    function handleResize() {
        var clientHeight = doc.clientHeight;
        if (clientHeight === prevClientHeight) return;
        requestAnimationFrame(function updateViewportHeight() {
            doc.style.setProperty(customVar, (clientHeight * 0.01) + 'px');
            prevClientHeight = clientHeight;
        });
    }

    handleResize();
    return handleResize;
}
/**
 * Contiene istruzioni per la sicurezza
 */
let sicurezza = {
    /**
     * Ritorna l'id univoco del browser
     */
    getUid: function () {
        return md5(newGuid());
    },
    getPin: function(length =4)
    {

        let ris = "";
        for(let i = 0; i < length; i++)
        {
            ris +=Math.floor((Math.random() * 9) + 1);
        }
        return ris;
    }
}
/**
 * Contiene istruzioni che determinano le dimensioni a video
 */
let schermo = {
    /**
     * Ritrona la larghezza in px dello schermo
     */
    getLarghezzaSchermo: function () {
        try {
            var viewportWidth = window.innerWidth || document.documentElement.clientWidth;
            return viewportWidth;
        } catch (ex) {
            console.error(ex.message);
        }

    },
    /**
     * Ritrona l'altezza in px dello schermo
     */
    getAltezzaSchermo: function () {
        try {

            var viewportHeight = window.innerHeight || document.documentElement.clientHeight;
            return viewportHeight;
        } catch (ex) {
            console.error(ex.message);
        }

    },
    /**
     * Ritrona l'altezza in px di un determinato oggetto
     * @param {string} "L'id dell'oggetto"
     */
    getAltezzaId: function (id) {
        try {

            var div = document.getElementById(id);
            return div.offsetHeight;
        } catch (ex) {
            console.error(ex.message);
        }

    },
    /**
     * Ritrona la larghezza in px di un determinato oggetto
     * @param {string} "L'id dell'oggetto"
     */
    getLarghezzaId: function (id) {
        try {
            var div = document.getElementById(id);
            return div.offsetWidth;
        } catch (ex) {
            console.error(ex.message);
        }

    }
}
/**
 * Contiene istruzioni che ti permettono di gestire un array
 */
let controlloArray = {
    /**
     * Rimuove un determinato indice da un array e lo ritorna
     * @param {array} "L'array dal quale rimuovere l'ogetto"
     * @param {int} "L'indice da rimuovere"
     */
    rimuoviIndice: function (array, indice) {
        try {
            array.splice(indice, 1);
            return array;
        } catch (ex) {
            console.error(ex)
        }

    },
    /**
     * Rimuove un determinato ogetto da un array e lo ritorna
     * @param {array} "L'array dal quale rimuovere l'ogetto"
     * @param {int} "L'oggetto da rimuovere"
     */
    rimuoviOggetto: function (array, valore) {
        try {
            var index = array.indexOf(valore);
            if (index > -1) {
                array.splice(index, 1);
            }
            return array;
        } catch (ex) {
            console.error(ex);
        }
    }
}
function getBase64Image(img) {
    var canvas = document.createElement("canvas");
    canvas.width = 300;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
  }
function rimuoviHtml(html) {
    let tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
}
/**
 * Verifica se un valore è vuoto
 * ritorna vero se il valore è vuoto
 * @param {any} "Valore da verificare"
 */
function isEmpty(value) {
    try {
        if (value === null || value === "null" || value === "undefined" || value === undefined || value === '' || value.length === 0 || value === NaN || value === 'NaN') {
            return true;
        }
    } catch (ex) {
        console.error(ex.message);
    }
    return false;
}
/**
 * Trasforma una stringa in valore booleano
 * @param {string} "Valore da convertire"
 */
function stringABoolean(string) {
    if(isEmpty(string))
    {
        return false;
    }
    switch (string.toString().toLowerCase().trim()) {
        case "true":
        case "yes":
        case "1":
            return true;
        case "false":
        case "no":
        case "0":
        case null:
            return false;
        default:
            return Boolean(string);
    }
}

function booleanToNumber(string) {
    if (!isEmpty(string)) {
        switch (string.toString().toLowerCase().trim()) {
            case "true":
            case "yes":
            case "1":
                return 1;
            case "false":
            case "no":
            case "0":
            case null:
                return 0;
            default:
                return Boolean(string);
        }
    }
}

function download(dataurl, filename) {

    let a = document.createElement("a");
    a.href = dataurl;
    a.target = "_blank";
    a.setAttribute("download", filename);
    a.setAttribute("target", "_blank");
    a.click();
}
/** Crea un header per raiseup */
function createHeader(noCors = true) {
    var header ={};
    if(noCors)
    {
         header = {
            'Authorization': undefined,
            'pragma': undefined,
            'cache-control': undefined,
            'if-modified-since': undefined,


        };
    }

    if (!isEmpty(localStorage["token"])) {
        header["token"] = CryptoJson.stringDecrypt(localStorage["token"]);
    } else {
        header["token"] = "raiseup";
    }
    if (!isEmpty(localStorage["dev"])) {
        header["dev"] = localStorage["dev"];
    }
    if (!isEmpty(localStorage["ipprivato"])) {
        header["ipprivato"] = localStorage["ipprivato"]
    }
    if (!isEmpty(localStorage["dev"])) {
        header["dev"] = localStorage["dev"]
    }
    return header;
}

/**
 * Crea un nuovo Guid
 */
function newGuid() {
    var sGuid = "";
    for (var i = 0; i < 32; i++) {
        sGuid += Math.floor(Math.random() * 0xF).toString(0xF);
    }
    return sGuid;
}
/** Copia un valore passato per indirizzo
 */
function copiaOggetto(obj) {

    var a = JSON.stringify(obj);
    return JSON.parse(a);
}

function copiaTesto(id) {
    /* Get the text field */

    var copyText = document.getElementById(id);


    copyText.select();
    copyText.setSelectionRange(0, 99999);

    navigator.clipboard.writeText(copyText.value);


}
/**
 * Inizializza un elemento audio tramite il suo id
 * @param {string} "Id dell'elemento da inizializzare"
 */
function getAudioElement(id) {
    const el = document.getElementById(id);
    if (!(el instanceof HTMLAudioElement)) {
        throw new Error(`Non esiste un elemento audio con id: "${id}"`);
    }
    return el;
}

let contorlloNumeri = {
    seIntero: function (numero) {
        try {
            if (!isEmpty(numero)) {
                if (numero === parseInt(numero, 10)) {
                    return true;
                }
            }
            return false;
        } catch (error) {
            console.error(error);
        }
    },
    seNumerico: function (carattere) {
        try {
            if (!isEmpty(carattere)) {
                if (!isNaN(carattere)) {
                    return true;
                }
            }
            return false;
        } catch (error) {
            console.error(error);
        }
    }
}
let math = {
    calcolaPercentuale: function (tot, num) {
        return ((num / tot) * 100).toFixed(0);
    }
}
let validazione = {
    "set": function (id, valore) {
        try {
            let input = document.getElementById(id);
            if(!isEmpty(input))
            {
                input.setCustomValidity(valore);
                input.reportValidity();
            }

        } catch (error) {
            console.error(error)
        }

    },
    'rimuovi': function (id) {
        try {
            let input = document.getElementById(id);
            input.setCustomValidity('');

        } catch (error) {
            console.error(error)
        }

    }
}
function clearText(str) {
    re = /\$|,|@|#|~|`|\%|\*|\^|\&|\(|\)|\+|\=|\[|\/|\-|\_|\]|\à|\è|\é|\@|\#|\&|\§|\£|\[|\}|\{|\;|\:|\'|\"|\<|\>|\?|\||\\|\°|\ç|\ò|\ù|\ì|\!|\$|\./g;
    // rimuove caratteri speciali come "$" e "," etc...
    return str.replace(re, "");
};
function initArray(lenght, value) {
    return new Array(lenght).fill(value);
}

let gestioneDate = {
    isValidDate: (d) => {
        return d instanceof Date && !isNaN(d);
    },
    dateOld: function (dat) {
        let dOld = new Date(dat);
        let dNew = new Date();

        var diff = (dNew.valueOf() - dOld.valueOf()) / 1000;

        let secondi = diff;
        if (secondi < 60) {
            return 30 + "s"
        } else {
            let minuti = diff / 60;

            if (minuti < 60) {
                return parseInt(minuti) + "m";
            } else {
                let ore = minuti / 60;
                if (ore < 24) {
                    return parseInt(ore) + "h"
                } else {
                    return dOld.getDate() + " " + gestioneDate.dataToMese(dOld.getMonth(), false)
                }

            }
        }
    },
    dataToMese: function (mese, short = false) {
        switch (mese) {
            case 0:
                if (short) {
                    return "Gen";
                } else {
                    return "Gennaio";
                }
                case 1:
                    if (short) {
                        return "Feb";
                    } else {
                        return "Febbraio";
                    }

                    case 2:
                        if (short) {
                            return "Mar";
                        } else {
                            return "Marzo";
                        }

                        case 3:
                            if (short) {
                                return "Apr";
                            } else {
                                return "Aprile";
                            }
                            case 4:
                                if (short) {
                                    return "Mag";
                                } else {
                                    return "Maggio";
                                }
                                case 5:
                                    if (short) {
                                        return "Giu";
                                    } else {
                                        return "Giugno";
                                    }
                                    case 6:
                                        if (short) {
                                            return "Lug";
                                        } else {
                                            return "Luglio";
                                        }
                                        case 7:
                                            if (short) {
                                                return "Ago";
                                            } else {
                                                return "Agosto";
                                            }
                                            case 8:
                                                if (short) {
                                                    return "Set";
                                                } else {
                                                    return "Settembre";
                                                }
                                                case 9:
                                                    if (short) {
                                                        return "Ott";
                                                    } else {
                                                        return "Ottobre";
                                                    }
                                                    case 10:
                                                        if (short) {
                                                            return "Nov";
                                                        } else {
                                                            return "Novembre";
                                                        }
                                                        case 11:
                                                            if (short) {
                                                                return "Dic";
                                                            } else {
                                                                return "Dicembre";
                                                            }

        }

    },
    dataToITA: function (data, time = false) {
        try {
            if (isEmpty(data)) {
                return "";
            }
            let d;
            if (contorlloNumeri.seNumerico(data) && !data._isValid) {

                d = new Date(data * 1000);
            } else {
                d = new Date(data);
            }

            if (!gestioneDate.isValidDate(d)) {
                return data;
            }

            let giorno = "0" + d.getDate();
            let mese = "0" + (d.getMonth() + 1);
            let anno = d.getFullYear();
            let ore = "0" + d.getHours();
            let minuti = "0" + d.getMinutes();
            let secondi = "0" + d.getSeconds();

            let dat =  giorno.slice(-2) + "/" + mese.slice(-2) + "/" + anno ;
            if(time)
            {
                dat +=" " + ore.slice(-2) + ':' + minuti.slice(-2) ;
            }
            return dat;



        } catch (error) {
            console.error(error);
        }
    },

    dataItToEn: function (data) {
        try {
            if (!isEmpty(data)) {
                let d = data.split("/");
                if (d.length > 1) {
                    return `${d[1]}-${d[0]}-${d[2]}`;
                } else {
                    return data;

                }


            }
        } catch (error) {
            console.error(error);
        }
    },
    dataTimeItToEn: function (data) {
        try {
            if (!isEmpty(data)) {
                let dat = data.split(" ");
                let d = dat[0].split("/");
                let ora = dat[1];

                if (d.length > 1) {
                    return `${d[1]}-${d[0]}-${d[2]} ${ora}`;
                } else {
                    return data;

                }


            }
        } catch (error) {
            console.error(error);
        }
    },
    minToOre :function (secT) {
        minuti = secT % 60;
        ore = (secT - minuti) / 60 % 60;
        if (ore < 10) {
            ore = "0" + ore;
        }
        if (minuti < 10) {
            minuti = "0" + minuti;
        }
        return ore + ":" + minuti;
    },
    oreToMinuti: function (orario) {
        try {
            if (isEmpty(orario)) {
                return "";
            }
            let time = orario.split(":");
            let ore = parseFloat(time[0]) * 60;
            let risposta = ore + parseFloat(time[1]);
            return risposta;

        } catch (error) {
            console.error(error);
        }
    },
    addMinToOre: function (orario, minuti) {
        try {
            if (!isEmpty(orario) && !isEmpty(minuti)) {
                let splitora1 = orario.split(":");
                let ora = parseInt(splitora1[0]);
                let min = parseInt(splitora1[1]);
                minuti = min + parseInt(minuti);
                while (minuti >= 60) {
                    minuti = minuti - 60;
                    ora++;
                    if (ora > 24) {
                        ora = 1;
                    }
                }
                let oracalcolata = "";
                if (ora < 10) {
                    oracalcolata = "0" + ora + ":";
                } else {
                    oracalcolata = ora + ":";
                }
                if (minuti < 10) {
                    oracalcolata += "0" + minuti;
                } else {
                    oracalcolata += minuti;
                }
                return oracalcolata;
            } else {
                return orario;
            }
        } catch (error) {
            console.error(error);
        }

    },
    sottMinToOre: function (orario, minuti) {
        try {
            if (!isEmpty(orario) && !isEmpty(minuti)) {
                let splitora1 = orario.split(":");
                let ora = parseInt(splitora1[0]);
                let min = parseInt(splitora1[1]);
                while (minuti >= 60) {
                    minuti = minuti - 60;
                    ora--;
                    if (ora < 0) {
                        ora = 23;
                    }
                }
                if (min >= minuti) {
                    min = min - minuti;
                } else {
                    minuti = minuti - min;
                    min = 0;
                    ora--;
                    if (ora < 0) {
                        ora = 23;
                    }
                    min = 60 - minuti;
                }
                let oracalcolata = "";
                if (ora < 10) {
                    oracalcolata = "0" + ora + ":";
                } else {
                    oracalcolata = ora + ":";
                }
                if (min < 10) {
                    oracalcolata += "0" + min;
                } else {
                    oracalcolata += min;
                }
                return oracalcolata;
            }
        } catch (error) {
            console.error(error);
        }
    }

}
function openContract(path) {
    // Apertura contratto se viene trovato POD,PDR,CODICE MIGRAZIONE
    try {
      let href = window.location.origin;
      window.location.href = href + path;
    } catch (ex) {
      console.log(ex);
    }
  }
  function OBJtoXML(obj) {
    var xml = '';
    for (var prop in obj) {
      xml += obj[prop] instanceof Array ? '' : "<" + prop + ">";
      if (obj[prop] instanceof Array) {
        for (var array in obj[prop]) {
          xml += "<" + prop + ">";
          xml += OBJtoXML(new Object(obj[prop][array]));
          xml += "</" + prop + ">";
        }
      } else if (typeof obj[prop] == "object") {
        xml += OBJtoXML(new Object(obj[prop]));
      } else {
        xml += obj[prop];
      }
      xml += obj[prop] instanceof Array ? '' : "</" + prop + ">";
    }
    var xml = xml.replace(/<\/?[0-9]{1,}>/g, '');
    return xml
  }
  function getCursorPos(id) {
    var input = document.getElementById(id);
    
    if ("selectionStart" in input && document.activeElement == input) {
        return {
            start: input.selectionStart,
            end: input.selectionEnd
        };
    }
    else if (input.createTextRange) {
        var sel = document.selection.createRange();
        if (sel.parentElement() === input) {
            var rng = input.createTextRange();
            rng.moveToBookmark(sel.getBookmark());
            for (var len = 0;
                     rng.compareEndPoints("EndToStart", rng) > 0;
                     rng.moveEnd("character", -1)) {
                len++;
            }
            rng.setEndPoint("StartToStart", input.createTextRange());
            for (var pos = { start: 0, end: len };
                     rng.compareEndPoints("EndToStart", rng) > 0;
                     rng.moveEnd("character", -1)) {
                pos.start++;
                pos.end++;
            }
            return pos;
        }
    }
    return -1;
}