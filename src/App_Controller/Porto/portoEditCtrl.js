﻿app.controller('portoEditCtrl', function ($scope, $rootScope, $http, $log, $routeParams, $timeout) {

    $scope.type = $routeParams.type;

    $scope.submitFormEdit = function () {

        Swal.fire({
            title: 'Modifica dati utente.',
            text: 'Vuoi continuare?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Sì',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.isConfirmed) {

                $scope.dati.dataNascita = $('#dataNascita').val();
                $scope.dati.dataEmissionePorto = $('#dataEmissioneTitolo').val();
                $scope.dati.dataCertificato = $('#dataCertificato').val();

                let cittaNascita = $('#localita').val();
                let data_diffida = $('#dataDiffida').val();

                $timeout(function () {

                    $scope.dati.luogoNascita = cittaNascita.replace(/\s*\(.*?\)\s*/g, '');

                    // Gestione Anni scadenza

                    let anniTitolo;
                    let anniCertificato;

                    switch ($scope.dati.titoloPosseduto) {
                        case "1":
                            anniCertificato = 5;
                            break;

                        case "2":
                        case "3":
                            anniTitolo = 5;
                            anniCertificato = 5;
                            break;

                        case "4":
                            anniTitolo = 1;
                            anniCertificato = 5;
                            break;


                        case "5":
                            anniTitolo = 2;
                            anniCertificato = 5;
                            break;
                    }

                    if ($scope.dati.titoloPosseduto != "1") {
                        let dataDiPartenzaPorto = moment($scope.dati.dataEmissionePorto);
                        let dataAggiornataPorto = dataDiPartenzaPorto.add(anniTitolo, 'years');
                        $scope.dati.scadenzaTitolo = dataAggiornataPorto.format('YYYY-MM-DD');

                    } else {
                        $scope.dati.dataEmissionePorto = null;
                        $scope.dati.scadenzaTitolo = null;
                    }

                  

                    // Calcola la differenza in giorni tra le due date
                    // $scope.dati.giorniScadenzaTitolo = moment($scope.dati.scadenzaTitolo).diff(moment($scope.dati.dataEmissionePorto), 'days');
                    // $scope.dati.giorniscadenzaCertificato = moment($scope.dati.scadenzaCertificato).diff(moment($scope.dati.dataCertificato), 'days');

                    $scope.dati.dataInserimento = moment().format('YYYY-MM-DD HH:mm:ss');

                    // Se diffidato
                    debugger
                    if ($scope.dati.diffida == 'si' && $scope.dati.scadenzaCertificato < data_diffida) {

                        $scope.dati.dataDiffida = data_diffida;

                        let dataMoment = moment(data_diffida).add(60, 'days').format('YYYY-MM-DD');

                        $scope.dati.dataFineDiffida = dataMoment;

                    } else if ($scope.dati.diffida == 'si' && $scope.dati.scadenzaCertificato > data_diffida) {

                        $scope.dati.diffida = "no";
                        $scope.dati.dataDiffida = null;
                        $scope.dati.dataFineDiffida = null;
                    }

                    if (!isEmpty($scope.dati.dataCertificato) && $scope.dati.dataCertificato != 'Invalid date') {

                        let dataDiPartenzaCertificato = moment($scope.dati.dataCertificato);
                        let dataAggiornataCertificato = dataDiPartenzaCertificato.add(anniCertificato, 'years');
                        $scope.dati.scadenzaCertificato = dataAggiornataCertificato.format('YYYY-MM-DD');

                       
                    } else {
                        $scope.dati.dataCertificato = null;
                        $scope.dati.scadenzaCertificato  = null;
                    }






                    $http.post('http://localhost:3000/editUtente', $scope.dati)
                        .then(function (response) {
                            console.log('Dati inseriti con successo:', response.data);
                            if (response.data.success) {
                                Swal.fire({
                                    title: 'Dati salvati correttamente!',
                                    text: '',
                                    icon: 'success',
                                    showConfirmButton: true,
                                    timer: 2000
                                })

                                $scope.load();

                            }
                        })
                        .catch(function (error) {
                            console.error('Errore durante l\'invio dei dati al server:', error);
                        });



                }, 500)
            }
        });



    };


    $scope.ckScaduto = function () {
        if (!$scope.dati) {
            return { scadutoP: false, scadutoC: false, text: "" };
        }

        const oggi = moment().format('YYYY-MM-DD');
        const scadutoC = $scope.dati.scadenzaCertificato < oggi;
        const scadutoP = $scope.dati.scadenzaTitolo < oggi;

        let text = "";
        if (scadutoC && scadutoP) {
            text = "Certificato e Titolo";
        } else if (scadutoC && !scadutoP) {
            text = "Certificato scaduto";
        } else if (!scadutoC && scadutoP) {
            text = "Titolo scaduto";
        }

        return { scadutoP, scadutoC, text };
    };

    $scope.setArchiviato = function (id) {


        Swal.fire({
            title: "Scegli la motivazione",
            input: "select",
            inputOptions: {
                "deceduto": "Deceduto",
                "no_rinnovo": "Non vuole rinnovare",
                "altro": "Altro"
            },
            inputPlaceholder: "Seleziona",
            showCancelButton: true,
            inputValidator: (value) => {
                return new Promise((resolve) => {


                    $http.get('http://localhost:3000/setArchiviato?id=' + id + '&motivo=' + value).then(function (response) {

                        console.log('Dati recuperati con successo:', response.data);

                        Swal.fire({
                            title: 'Dati salvati correttamente!',
                            text: ' ',
                            icon: 'success',
                            showConfirmButton: false,

                            timer: 2000
                        })


                    })
                        .catch(function (error) {
                            console.error('Errore durante il recupero dei dati:', error);
                        });

                });
            }
        });


    }

    $scope.load = function () {

        let idUtente = $routeParams.id;

        $http.get('http://localhost:3000/utente?id=' + idUtente)
            .then(function (response) {

                console.log('Dati recuperati con successo:', response.data);

                $scope.dati = response.data;

                $timeout(function () {

                    $('#dataNascita').val($scope.dati.dataNascita);
                    $('#dataEmissioneTitolo').val($scope.dati.dataRilascioPorto);
                    $('#dataCertificato').val($scope.dati.dataRilascioCertificato);
                    $('#dataDiffida').val($scope.dati.dataDiffida);

                    $('#localita').val($scope.dati.luogoNascita + ' (' + $scope.dati.provinciaNascita + ')');


                    // $('#localita').trigger('input');

                }, 200)

            })
            .catch(function (error) {
                console.error('Errore durante il recupero dei dati:', error);
            });


        $('#localita').typeahead({
            displayText: function (item) {
                return item.nome + ' (' + item.sigla + ')'
            },
            afterSelect: function (item) {

                this.$element[0].value = item.nome + ' (' + item.sigla + ')'

                $timeout(function () {

                    $scope.dati.provinciaNascita = item.sigla;

                }, 100)
            },
            source: function (query, process) {
                return $.getJSON('App_Controller/Porto/comuni.json', {
                    query: query
                }, function (data) {
                    process(data);

                })
            }
        });
    }
});