app.directive("select2", function ($timeout, $parse, $rootScope) {
    return {
        restrict: 'AE',
        require: 'ngModel',
        scope: { data: "=ngModel" },
        link: function (scope, element, attrs, ngModel, search) {

            $timeout(function () {


                let placeholder = attrs.placeholder;
                let css = attrs.class;

                if (isEmpty(placeholder)) {
                    placeholder = " ";
                }
                if (isEmpty(css)) {
                    css = "form-select "
                }
                let arr = [];
                if (!isEmpty(attrs.data)) {
                    arr = JSON.parse(attrs.data);
                }

                let val = ngModel.$modelValue;

                let multiple = false;
                if (!attrs.multiple) {
                    if (isEmpty(val)) {
                        $(element).val('')
                        if (attrs.noemptyvalue != "1") {
                            $(element)[0][0].outerHTML = `<option value="" > </option>`
                        }

                    }

                }
                else {
                    multiple = true;
                }

                if (!isEmpty(attrs.color) || attrs.color == true) {
                    // Con colore usato per select degli stati
                    var option = {
                        placeholder: placeholder,
                        allowClear: true,

                        containerCssClass: css + " arrow-none",
                        language: $rootScope.convertLenguage($rootScope.lenguage).lngShort,
                        data: arr,
                        templateResult: function (option) {
                            if (!option.id) {
                                return option.text;
                            }

                            var $option = $('<span>' + option.text + '</span>');

                            $option.css('color', option.color);

                            return $option;
                        }
                    };
                } else {

                    // Tutte le select 
                    var option = {

                        placeholder: placeholder,
                        allowClear: true,
                        containerCssClass: css + " arrow-none",
                        language: $rootScope.convertLenguage($rootScope.lenguage).lngShort,
                        data: arr
                    };
                }


                if (!isEmpty(attrs.maxlength)) {

                    option.maximumSelectionLength = parseInt(attrs.maxlength);
                }

                let modal = attrs.modalid;

                if (!isEmpty(modal)) {
                    option["dropdownParent"] = "#" + modal;
                }

                if ($(element).data('select2')) {

                    $(element).select2('destroy');
                }
               
                $(element).select2(option);

                if (isEmpty(val)) {

                    $(element).val(val).trigger('change');
                    ngModel.$setViewValue('');
                }
                else {

                    $(element).val(val).trigger('change')
                    ngModel.$setViewValue(val);
                }

                $(element).on('change', function (e) {

                    ngModel.$setViewValue($(element).val());

                });

                $(element).on('select2:select', function (e) {


                    ngModel.$setViewValue($(element).val());

                });

                $(element).on('select2:unselect', function (e) {


                    ngModel.$setViewValue($(element).val());

                });

                $(element).on('select2:clear', function (e) {


                    if (multiple) {
                        $(element).val([]).trigger('change')
                        ngModel.$setViewValue([]);
                    }
                    else {
                        $(element).val('').trigger('change')
                        ngModel.$setViewValue('');
                    }

                });

            });

        }
    };
});