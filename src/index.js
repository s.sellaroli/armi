const { app: electronApp, BrowserWindow } = require('electron');
process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = true
const express = require('express');
const app = express();

const getRoutes = require('./getRoutes'); // Importa le route GET
const path = require('path')

// Middleware per il parsing dei dati JSON
app.use(express.json());

// Utilizza le route GET
app.use('/', getRoutes); // Modifica il percorso se necessario


app.listen(3000, function () {
  console.log('Server avviato sulla porta 3000');
});

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) {
  app.quit();
}

const createWindow = () => {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 1250,
    height: 850,
    minWidth: 1250,  // Imposta la larghezza minima
    minHeight: 850, // Imposta l'altezza minima
    resizable: true,
    frame: true, // Disabilita menu
    autoHideMenuBar: true,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
    },
  });

  // and load the index.html of the app.
  mainWindow.loadFile(path.join(__dirname, 'index.html'));

  mainWindow.on('resize', () => {
    const [width, height] = mainWindow.getSize();
    const [minWidth, minHeight] = mainWindow.getMinimumSize();

    // Se la larghezza o l'altezza sono inferiori alla dimensione minima, reimposta la dimensione della finestra
    if (width < minWidth || height < minHeight) {
      mainWindow.setSize(Math.max(width, minWidth), Math.max(height, minHeight));
    }
  });
  // Open the DevTools.
  // mainWindow.webContents.openDevTools();
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
electronApp.on('ready', createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
electronApp.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    electronApp.quit();
  }
});

electronApp.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
