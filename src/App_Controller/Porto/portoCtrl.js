﻿app.controller('portoCtrl', function ($scope, $rootScope, $http, $log, $routeParams, $timeout) {

    $scope.submitForm = function () {

        $scope.dati.dataNascita = $('#dataNascita').val();
        $scope.dati.dataEmissionePorto = $('#dataEmissionePorto').val();
        $scope.dati.dataCertificato = $('#dataCertificato').val();

        let cittaNascita = $('#localita').val();

        $timeout(function () {

            $scope.dati.luogoNascita = cittaNascita.replace(/\s*\(.*?\)\s*/g, '');

            // Gestione Anni scadenza

            let anniTitolo;
            let anniCertificato;

            switch ($scope.dati.titoloPosseduto) {
                case "1":
                    anniCertificato = 5;
                    break;

                case "2":
                case "3":
                    anniTitolo = 5;
                    anniCertificato = 5;
                    break;

                case "4":
                    anniTitolo = 1;
                    anniCertificato = 5;
                    break;


                case "5":
                    anniTitolo = 2;
                    anniCertificato = 5;
                    break;
            }

            if ($scope.dati.titoloPosseduto != "1") {
                let dataDiPartenzaPorto = moment($scope.dati.dataEmissionePorto);
                let dataAggiornataPorto = dataDiPartenzaPorto.add(anniTitolo, 'years');

                $scope.dati.scadenzaTitolo = dataAggiornataPorto.format('YYYY-MM-DD');


            } else {
                $scope.dati.dataEmissionePorto = null;
                $scope.dati.scadenzaTitolo = null;
            }
            debugger
            if (!isEmpty($scope.dati.dataCertificato)) {

                let dataDiPartenzaCertificato = moment($scope.dati.dataCertificato);
                let dataAggiornataCertificato = dataDiPartenzaCertificato.add(anniCertificato, 'years');
                $scope.dati.scadenzaCertificato = dataAggiornataCertificato.format('YYYY-MM-DD');
            } else {
                $scope.dati.dataCertificato = null;
            }



            //Calcola la differenza in giorni tra le due date
            //$scope.dati.giorniScadenzaTitolo = moment($scope.dati.scadenzaTitolo).diff(moment($scope.dati.dataEmissionePorto), 'days');
            //$scope.dati.giorniscadenzaCertificato = moment($scope.dati.scadenzaCertificato).diff(moment($scope.dati.dataCertificato), 'days');

            $scope.dati.dataInserimento = moment().format('YYYY-MM-DD HH:mm:ss');

            $http.post('http://localhost:3000/insert', $scope.dati)
                .then(function (response) {
                    console.log('Dati inseriti con successo:', response.data);
                    // Pulisci il form dopo l'inserimento dei dati
                    $scope.dati = {};

                    if (response.data.success) {
                        Swal.fire({
                            title: 'Dati salvati correttamente!',
                            text: '',
                            icon: 'success',
                            showConfirmButton: true,
                            timer: 2000
                        })

                    }



                })
                .catch(function (error) {
                    console.error('Errore durante l\'invio dei dati al server:', error);
                });



        }, 500)
    };


    $scope.load = function () {

        $('#localita').typeahead({
            displayText: function (item) {
                return item.nome + ' (' + item.sigla + ')'
            },
            afterSelect: function (item) {
                this.$element[0].value = item.nome + ' (' + item.sigla + ')'

                $scope.dati.provinciaNascita = item.sigla;

            },
            source: function (query, process) {
                return $.getJSON('App_Controller/Porto/comuni.json', {
                    query: query
                }, function (data) {
                    process(data)
                })
            }
        });
    }


});