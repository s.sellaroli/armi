// getRoutes.js

const express = require('express');
const router = express.Router();
const sqlite3 = require('sqlite3').verbose();

const path = require('path');
const dbPath = path.join(__dirname, 'DB/database.db');

const db = new sqlite3.Database(dbPath);

// Inserisce una nuova anagrafica
router.post('/insert', function (req, res) {

    const userData = req.body;

    db.run(`INSERT INTO utenti (cognome, nome, luogoNascita, provinciaNascita, dataNascita, cellulare1, cellulare2, titoloPosseduto, numeroPorto, dataRilascioPorto, possessoreArmi, dataRilascioCertificato,scadenzaTitolo,scadenzaCertificato,giorniScadenzaTitolo,giorniscadenzaCertificato,dataInserimento,nota,diffida) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`, [userData.cognome, userData.nome, userData.luogoNascita, userData.provinciaNascita, userData.dataNascita, userData.cellulare1, userData.cellulare2, userData.titoloPosseduto, userData.numeroPorto, userData.dataEmissionePorto, userData.possessoreArmi, userData.dataCertificato, userData.scadenzaTitolo, userData.scadenzaCertificato, userData.giorniScadenzaTitolo, userData.giorniscadenzaCertificato, userData.dataInserimento, userData.nota,'no'], function (err) {
        if (err) {
            return res.status(500).send({ error: 'Errore durante l\'inserimento dei dati nel database' });
        }
        res.send({ success: true, message: 'Dati inseriti con successo nel database' });
    });
});
// Modifica anagrafica 
router.post('/editUtente', function (req, res) {

    const userData = req.body;
    console.log('Dati ricevuti dal client:', userData);

    db.run('UPDATE utenti SET cognome = ?, nome = ?, luogoNascita = ?, provinciaNascita = ?, dataNascita = ?, cellulare1 = ?, cellulare2 = ?, titoloPosseduto = ?, numeroPorto = ?, dataRilascioPorto = ?, possessoreArmi = ?, dataRilascioCertificato = ?, scadenzaTitolo = ?, scadenzaCertificato = ?, giorniScadenzaTitolo = ?, giorniscadenzaCertificato = ?, dataEdit = ?, nota = ?, diffida = ?, dataDiffida = ?, dataFineDiffida = ? WHERE id_utente = ?', [userData.cognome, userData.nome, userData.luogoNascita, userData.provinciaNascita, userData.dataNascita, userData.cellulare1, userData.cellulare2, userData.titoloPosseduto, userData.numeroPorto, userData.dataEmissionePorto, userData.possessoreArmi, userData.dataCertificato, userData.scadenzaTitolo, userData.scadenzaCertificato, userData.giorniScadenzaTitolo, userData.giorniscadenzaCertificato, userData.dataInserimento, userData.nota, userData.diffida, userData.dataDiffida, userData.dataFineDiffida, userData.id_utente], function (err) {

        if (err) {
            return res.status(500).send({ error: 'Errore durante l\'aggiornamento dei dati nel database' });
        }
        res.send({ success: true });
    });
});
// Recupera tutti gli utenti
router.get('/utenti', function (req, res) {

    db.all('SELECT * FROM utenti WHERE archiviato is NULL', function (err, rows) {
        if (err) {
            return res.status(500).send({ error: 'Errore durante il recupero dei dati dal database' });
        }
        res.send(rows);
    });
});
// Recupera singolo utente
router.get('/utente', function (req, res) {

    const userId = req.query.id; // Ottieni l'ID dell'utente dalla query della richiesta URL

    db.get('SELECT * FROM utenti WHERE id_utente = ?', [userId], function (err, row) {
        if (err) {
            return res.status(500).send({ error: 'Errore durante il recupero dei dati dal database' });
        }
        if (!row) {
            return res.status(404).send({ error: 'Utente non trovato' });
        }
        res.send(row); // Invia i dati dell'utente trovato nella risposta
    });
});
// Dati per grafico a torta
router.get('/graficoPie', function (req, res) {

    db.all('SELECT titoloPosseduto, COUNT(*) AS conteggio FROM utenti WHERE  archiviato is NULL GROUP BY titoloPosseduto ', function (err, rows) {
        if (err) {
            return res.status(500).send({ error: 'Errore durante il recupero dei dati dal database' });
        }
        res.send(rows);
    });
});
// Recupera utenti con certificato/Porto scaduto
router.get('/Scaduti', function (req, res) {

    const today = new Date().toISOString().split('T')[0];

    const campo = req.query.tipo;

    db.all(`SELECT * FROM utenti WHERE ${campo} < '${today}' AND archiviato is NULL`, function (err, rows) {
        if (err) {
            return res.status(500).send({ error: 'Errore durante il recupero dei dati dal database' });
        }
        res.send(rows);
    });

});
// Recupero il conteggio delle scadenze per le card
router.get('/conteggioScadenze', function (req, res) {

    const today = new Date().toISOString().split('T')[0]; // Ottieni la data corrente nel formato "YYYY-MM-DD"

    let queryCertificato = `SELECT COUNT(*) AS countCertificato FROM utenti WHERE scadenzaCertificato < '${today}' AND archiviato is NULL`;
    let queryTitolo = `SELECT COUNT(*) AS countTitolo FROM utenti WHERE scadenzaTitolo < '${today}' AND archiviato is NULL`;
    let queryDiffidati = `SELECT COUNT(*) AS countDiffidati FROM utenti WHERE diffida =  "si"  AND dataFineDiffida is NOT NULL AND dataDiffida is not NULL AND archiviato is NULL`;
    let queryDiffidatiScaduti = `SELECT COUNT(*) AS countDiffidatiScaduti FROM utenti WHERE diffida =  "si" AND dataFineDiffida < '${today}' AND archiviato is NULL`;

    let results = {};

    db.all(queryCertificato, function (err, rowsCertificato) {

        if (err) {
            return res.status(500).send({ error: 'Errore durante il recupero dei dati dal database' });
        }

        results.countCertificato = rowsCertificato[0].countCertificato;

        db.all(queryTitolo, function (err, rowsTitolo) {
            if (err) {
                return res.status(500).send({ error: 'Errore durante il recupero dei dati dal database' });
            }
            results.countTitolo = rowsTitolo[0].countTitolo;

            db.all(queryDiffidati, function (err, rowsDiffidati) {
                if (err) {
                    return res.status(500).send({ error: 'Errore durante il recupero dei dati dal database' });
                }
                results.countDiffidati = rowsDiffidati[0].countDiffidati;

                db.all(queryDiffidatiScaduti, function (err, rowsDiffidatiScaduti) {
                    if (err) {
                        return res.status(500).send({ error: 'Errore durante il recupero dei dati dal database' });
                    }
                    results.countDiffidatiScaduti = rowsDiffidatiScaduti[0].countDiffidatiScaduti;

                    res.send(results);
                });
            });
        });
    });
});
// Imposto un utente come archiviato
router.get('/setArchiviato', function (req, res) {

    const userId = req.query.id;
    const motivo = req.query.motivo;
    const currentDate = new Date().toISOString().slice(0, 19).replace('T', ' ');

    db.run('UPDATE utenti SET archiviato = ?, motivoArchiviato = ?, dataArchivio = ? WHERE id_utente = ?', [1, motivo, currentDate, userId], function (err) {
        if (err) {
            return res.status(500).send({ error: 'Errore durante l\'aggiornamento dei dati nel database' });
        }
        res.send({ success: true });
    });
});
// Recupera tutti i nominativi diffidati 
router.get('/diffidati', function (req, res) {

    db.all('SELECT * FROM utenti WHERE diffida = "si" AND dataFineDiffida is NOT NULL AND dataDiffida is not NULL AND archiviato is NULL ORDER BY dataDiffida DESC', function (err, rows) {
        if (err) {
            return res.status(500).send({ error: 'Errore durante il recupero dei dati dal database' });
        }
        res.send(rows);
    });
});
// Recupera tutti i nominativi diffidati e  scaduti
router.get('/diffidatiScaduti', function (req, res) {
    
    const today = new Date().toISOString().split('T')[0]; // Ottieni la data corrente nel formato "YYYY-MM-DD"

    db.all(`SELECT * FROM utenti WHERE diffida = "si" AND dataFineDiffida < '${today}' AND archiviato is NULL ORDER BY dataDiffida ASC`, function (err, rows) {
        if (err) {
            return res.status(500).send({ error: 'Errore durante il recupero dei dati dal database' });
        }
        res.send(rows);
    });
});













module.exports = router;