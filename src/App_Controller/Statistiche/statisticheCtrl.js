﻿app.controller('statisticheCtrl', function ($scope, $rootScope, $http, $log, $routeParams, $timeout) {



    $scope.userData = {};
    $scope.dati = {};

    $scope.submitForm = function () {


        // Effettua una richiesta POST al server Express
        $http.post('http://localhost:3000/insert', $scope.userData)
            .then(function (response) {
                console.log('Dati inseriti con successo:', response.data);
                // Pulisci il form dopo l'inserimento dei dati
                $scope.userData = {};
            })
            .catch(function (error) {
                console.error('Errore durante l\'invio dei dati al server:', error);
            });
    };


    $scope.load = function () {
        $http.get('http://localhost:3000/utenti')
            .then(function (response) {
                console.log('Dati recuperati con successo:', response.data);
                $scope.dati = response.data;
            })
            .catch(function (error) {
                console.error('Errore durante il recupero dei dati:', error);
            });
            $scope.loadScadenze();
    }
    $scope.loadID = function (id) {
        $http.get('http://localhost:3000/utente?id=' + id)
            .then(function (response) {
                console.log('Dati recuperati con successo:', response.data);
                debugger
                $scope.dati = [response.data];
            })
            .catch(function (error) {
                console.error('Errore durante il recupero dei dati:', error);
            });
    }

    $scope.loadPie = function () {

        $http.get('http://localhost:3000/graficoPie')
            .then(function (response) {
                console.log('Dati recuperati con successo:', response.data);
                $scope.graficoPie(response.data);
            })
            .catch(function (error) {
                console.error('Errore durante il recupero dei dati:', error);
            });
    }

    $scope.graficoPie = function (dati) {

        let arrayDati = [];

        let arrayLabel = [];

        let etichetta;

        for (let i = 0; i < dati.length; i++) {

            arrayDati.push(dati[i].conteggio);


            switch (dati[i].titoloPosseduto) {
                case "1":
                    etichetta = "Detenzione";
                    break;
                case "2":
                    etichetta = "Tiro a Volo";
                    break;
                case "3":
                    etichetta = "Uso Caccia";
                    break;
                case "4":
                    etichetta = "Difesa Personale";
                    break;
                case "5":
                    etichetta = "GPG";
                    break;

            }

            arrayLabel.push(etichetta + ' = ' + dati[i].conteggio);

        }

        // Configura il grafico
        const ctx = document.getElementById('myChart').getContext('2d');
        const myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: arrayLabel,
                datasets: [{
                    data: arrayDati,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.5)',
                        'rgba(54, 162, 235, 0.5)',
                        'rgba(255, 206, 86, 0.5)',
                        'rgba(75, 192, 192, 0.5)',
                        'rgba(153, 102, 255, 0.5)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                plugins: {
                    legend: {
                        position: 'right'
                    }
                }
            }
        });

    }

    $scope.loadScadenze = function () {

        $http.get('http://localhost:3000/conteggioScadenze')
            .then(function (response) {
                console.log('Dati recuperati con successo:', response.data);
                $scope.cardScadenze = response.data;
            })
            .catch(function (error) {
                console.error('Errore durante il recupero dei dati:', error);
            });
    }

});