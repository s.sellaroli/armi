﻿
app.directive('fileChange', ['$parse', function ($parse) {

    return {
        restrict: 'A',
        link: function ($scope, element, attrs) {
            let attrHandler = $parse(attrs['fileChange']);
            let handler = function (e) {
                $scope.$apply(function () {

                    attrHandler($scope, { $event: e, files: e.target.files });
                });
            };
            element[0].addEventListener('change', handler, false);
        }
    };
}]);
app.directive('disableCopy', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            if (scope.$eval(attrs.disableCopy)) {
                element.on('copy', function (event) {
                    event.preventDefault(); // Impedisce l'azione predefinita di copia
                });
            }
        }
    };
});
app.directive('jsmask', function ($timeout, $rootScope) {
    return {
        link: function (scope, element, attrs, ngModel) {
            $timeout(function () {

                var id = attrs.id;

                var data_format = attrs.dataformat;

                var pl = attrs.placeholder;

                $("#" + id).mask(data_format, { placeholder: pl },);

            });
        }
    }
});

angular.module('numfmt-error-module', [])

    .run(function ($rootScope) {
        $rootScope.typeOf = function (value) {
            return typeof value;
        };
    })

app.directive('stringToNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (value) {
                return '' + value;
            });
            ngModel.$formatters.push(function (value) {
                return parseFloat(value);
            });
        }
    };
});
app.directive('stringToBool', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (value) {
                return stringABoolean(value);
            });
            ngModel.$formatters.push(function (value) {
                return stringABoolean(value);
            });
        }
    };
});
app.directive('convertDate', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {

            ngModel.$parsers.push(function (val) {

                if (!isEmpty(val) && object.prototype.toString.call(val) != '[object Date]') {


                    if (val.split(":").length > 1) {
                        var a = new Date(1992, 0, 1, val.split(":")[0], val.split(":")[1]);
                    }
                    else {
                        if (val.length == 1) {
                            val = "0" + val;
                        }

                        val = val + ":00"
                        var a = new Date(1992, 0, 1, val.split(":")[0], val.split(":")[1]);
                    }

                    return a;
                }
                return val;
            });
            ngModel.$formatters.push(function (val) {
                if (!isEmpty(val) && object.prototype.toString.call(val) != '[object Date]') {


                    if (val.split(":").length > 1) {
                        var a = new Date(1992, 0, 1, val.split(":")[0], val.split(":")[1]);
                    }
                    else {
                        if (val.length == 1) {
                            val = "0" + val;
                        }

                        val = val + ":00"
                        var a = new Date(1992, 0, 1, val.split(":")[0], val.split(":")[1]);
                    }

                    return a;
                }
                return val;
            });
        }
    };
});
app.directive('convertToNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (val) {
                return parseInt(val, 10);
            });
            ngModel.$formatters.push(function (val) {
                return '' + val;
            });
        }
    };
});
app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngenter);
                });

                event.preventDefault();
            }
        });
    };
});
app.directive('convertToString', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (val) {
                return val.toString();
            });
            ngModel.$formatters.push(function (val) {
                return '' + val;
            });
        }
    };
});
app.directive('validatecustom', function () {
    return {
        restrict: "A",
        require: "?ngModel",
        link: function (scope, element, attrs, ngModel) {

            scope.$watch(attrs['ngModel'], function (v) {

                if (!isEmpty(v)) {
                    validazione.rimuovi(element[0].id)
                }

            });
        }
    };
});

app.directive('uppercase', function () {
    return {
        restrict: "A",
        require: "?ngModel",
        link: function (scope, element, attrs, ngModel) {
          
                //This part of the code manipulates the model
                debugger
                ngModel.$parsers.push(function (input) {

                    return input ? input.toUpperCase() : "";



                });

                //This part of the code manipulates the viewvalue of the element

                element.css("text-transform", "uppercase");
            
        }
    };
});

app.directive('capitalize', function () {
    return {
        restrict: "A",
        require: "?ngModel",
        link: function (scope, element, attrs, ngModel) {
            // This part of the code manipulates the model
            ngModel.$parsers.push(function (input) {
                if (input) {
                    return input.charAt(0).toUpperCase() + input.slice(1);
                }
                return "";
            });

            // This part of the code manipulates the view value of the element
            element.css("text-transform", "capitalize");
        }
    };
});

app.directive('solonumeri', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attrs, ctrl) {
            ctrl.$parsers.push(function (input) {
                if (input == undefined) return ''
                var inputNumber = input.toString().replace(/[^0-9]/g, '');
                if (inputNumber != input) {
                    ctrl.$setViewValue(inputNumber);
                    ctrl.$render();
                }
                return inputNumber;
            });
        }
    };
});
app.directive('autoTabTo', [function () {
    return {
        restrict: "A",
        link: function (scope, el, attrs) {
            el.bind('keyup', function (e) {

                if (this.value.length === parseInt(attrs.maxlength)) {
                    var element = document.getElementById(attrs.autoTabTo);
                    if (element)
                        element.focus();
                }
            });
        }
    }
}]);
app.directive('autoTabBack', [function () {
    return {
        restrict: "A",
        link: function (scope, el, attrs) {
            el.bind('keyup', function (e) {


                if (this.value.length === 0) {
                    var element = document.getElementById(attrs.autoTabBack);
                    if (element)
                        element.focus();
                }
            });
        }
    }
}]);
app.directive("tabautomatico", function () {
    return {
        restrict: "A",
        link: function ($scope, element) {

            element.on("input", function (e) {

                if (element.val().length == element.attr("maxlength")) {
                    var $nextElement = element.next();
                    if ($nextElement.length) {
                        $nextElement[0].focus();
                    }
                }
            });
        }
    }
});
app.directive("tabindietro", function () {
    return {
        restrict: "A",
        link: function ($scope, element) {
            element.on("input", function (e) {

                if (element.val().length == 0) {
                    var $nextElement = element.prev();
                    if ($nextElement.length) {
                        $nextElement[0].focus();
                    }
                }
            });
        }
    }
});
app.directive('autofocus', function ($timeout) {
    return {
        link: function (scope, element, attrs) {
            $timeout(function () {
                element.focus();
            });
        }
    }
});

app.directive('validate', function ($timeout) {

    return {
        restrict: 'AE',
        require: 'ngModel',

        link: function (scope, element, attrs, ngModel) {
            if (!ngModel) {
                return;
            }
            //initially set false validation of form
            ngModel.$setValidity('validate', false);

            ngModel.$parsers.push(function (val) {
                console.log(attrs)
                if (!isEmpty(val)) {


                    //metti class valid

                }
                else {
                    //rimuvoi class invalid
                }
            })
        }
    };

});


app.directive('ngDrop', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element[0].addEventListener('dragover', function (event) {
                event.preventDefault();
                element.addClass('drag-over');
            });

            element[0].addEventListener('dragleave', function (event) {
                event.preventDefault();
                element.removeClass('drag-over');
            });

            element[0].addEventListener('drop', function (event) {
                event.preventDefault();
                element.removeClass('drag-over');
                var files = event.dataTransfer.files;
                scope.$apply(function () {
                    scope.$eval(attrs.ngDrop, { $files: files });
                });
            });
        }
    };
});

app.directive('thousandsSeparator', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            ngModelCtrl.$parsers.push(function (viewValue) {
                // Rimuovi eventuali punti o virgole per assicurarti che il valore sia un numero
                var cleanValue = viewValue.replace(/[.,]/g, '');
                // Formatta il numero in migliaia
                var formattedValue = cleanValue.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
                // Aggiorna la vista con il valore formattato
                element.val(formattedValue);
                return formattedValue;
            });

            ngModelCtrl.$formatters.push(function (modelValue) {
                // Formatta il numero in migliaia
                var formattedValue = modelValue.replace(/\B(?=(\d{3})+(?!\d))/g, '.');
                return formattedValue;
            });
        }
    };
});

app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            ngModelCtrl.$parsers.push(function (inputValue) {
                // Rimuovi tutti i caratteri non numerici dal valore di input
                if (inputValue) {
                    var transformedInput = inputValue.replace(/[^0-9]/g, '');

                    if (transformedInput !== inputValue) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return '';
            });
        }
    };
})

app.directive('customNumberFormat', ['$filter', function($filter) {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
            ngModelCtrl.$parsers.push(function(viewValue) {
                // Rimuovi eventuali punti e virgole per la formattazione italiana
                var cleanValue = viewValue.replace(/[.,]/g, '');

                // Limita il numero a due decimali
                cleanValue = parseFloat(cleanValue).toFixed(2);

                // Formatta il numero con 2 decimali, virgola per i decimali e punto per le migliaia
                var formattedValue = cleanValue.replace(/\B(?=(\d{3})+(?!\d))/g, "+").replace('.', ',');

                // Aggiorna la vista con il valore formattato
                ngModelCtrl.$setViewValue(formattedValue);
                ngModelCtrl.$render();

                // Restituisci il valore formattato per il modello
                return formattedValue;
            });
        }
    };
}]);