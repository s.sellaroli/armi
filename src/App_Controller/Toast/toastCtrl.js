﻿app.controller('toastCtrl', function ($scope, $rootScope, $log, $routeParams, $timeout ) {

 $rootScope.notifiche = {
    "list":[],
    "add": function(titolo,testo,classe){
        let id =  newGuid();
       $rootScope.notifiche.list.push({
        "titolo":titolo,
        "testo":testo,
        "id":id,
        "class":classe

       });
       $timeout(function(){
        var myAlert =document.getElementById(id);//select id of toast
          var bsAlert = new bootstrap.Toast(myAlert);//inizialize it
          bsAlert.show();//show it
       },1)

    },
    "remove": function(id){

        var myAlert =document.getElementById(id);//select id of toast
          var bsAlert = new bootstrap.Toast(myAlert);//inizialize it
          bsAlert.hide();//show it

    }
 }

});