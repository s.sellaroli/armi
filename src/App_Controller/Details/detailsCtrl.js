﻿app.controller('detailsCtrl', function ($scope, $rootScope, $http, $log, $routeParams, $timeout) {


    $scope.type = $routeParams.type;
    $scope.campiDate = ['dataNascita', 'scadenzaTitolo', 'scadenzaCertificato', 'dataDiffida', 'dataFineDiffida'];
    $scope.datiTabella = [];
    $scope.load = function () {

        $scope.dati = [];
        $scope.nomeCampi = [];
        $scope.datiTabella = [];

        $scope.titoloTabella = "";

        switch ($scope.type) {
            case "1":
                $scope.loadAll();
                $scope.titoloTabella = "Nominativi Inseriti";
                $scope.nomeCampi = ['Cognome', 'Nome', 'Data di Nascita', 'Luogo di Nascita', 'Provincia', 'Titolo Posseduto', 'Scadenza Titolo', 'Scadenza Certificato'];
                $scope.nomeCella = ['cognome', 'nome', 'dataNascita', 'luogoNascita', 'provinciaNascita', 'titoloPosseduto', 'scadenzaTitolo', 'scadenzaCertificato'];
                break;
            case "2":
                $scope.loadScaduti('scadenzaCertificato');
                $scope.titoloTabella = "Certificati Scaduti";
                $scope.nomeCampi = ['Cognome', 'Nome', 'Cellulare', 'Data di Nascita', 'Luogo di Nascita', 'Provincia', 'Titolo Posseduto', 'Scadenza Certificato'];
                $scope.nomeCella = ['cognome', 'nome', 'cellulare1', 'dataNascita', 'luogoNascita', 'provinciaNascita', 'titoloPosseduto', 'scadenzaCertificato'];
                break;
            case "3":
                $scope.loadScaduti('scadenzaTitolo');
                $scope.titoloTabella = "Titoli Scaduti";
                $scope.nomeCampi = ['Cognome', 'Nome', 'Cellulare', 'Data di Nascita', 'Luogo di Nascita', 'Provincia', 'Titolo Posseduto', 'Scadenza Titolo'];
                $scope.nomeCella = ['cognome', 'nome', 'cellulare1', 'dataNascita', 'luogoNascita', 'provinciaNascita', 'titoloPosseduto', 'scadenzaTitolo'];
                break;
            case "4":
                $scope.loadDiffidati();
                $scope.titoloTabella = "Nominativi Diffidati";
                $scope.nomeCampi = ['Cognome', 'Nome', 'Cellulare', 'Data di Nascita', 'Luogo di Nascita', 'Provincia', 'Titolo Posseduto', 'Scadenza Titolo', 'Diffida'];
                $scope.nomeCella = ['cognome', 'nome', 'cellulare1', 'dataNascita', 'luogoNascita', 'provinciaNascita', 'titoloPosseduto', 'scadenzaTitolo', 'dataDiffida'];
                break;
            case "5":
                $scope.loadDiffidatiScaduti();
                $scope.titoloTabella = "Nominativi Diffidati Scaduta";
                $scope.nomeCampi = ['Cognome', 'Nome', 'Cellulare', 'Data di Nascita', 'Luogo di Nascita', 'Provincia', 'Titolo Posseduto', 'Scadenza Titolo', 'Diffida', 'Scadenza Diffida'];
                $scope.nomeCella = ['cognome', 'nome', 'cellulare1', 'dataNascita', 'luogoNascita', 'provinciaNascita', 'titoloPosseduto', 'scadenzaTitolo', 'dataDiffida', 'dataFineDiffida'];
                break;

        }
    }

    $scope.loadScaduti = function (type) {

        let a = type;

        $http.get('http://localhost:3000/Scaduti?tipo=' + a)
            .then(function (response) {

                console.log('Dati recuperati con successo:', response.data);

                $scope.datiTabella = response.data;
                $scope.totalPages = Math.ceil($scope.datiTabella.length / $scope.limitRow);
            })
            .catch(function (error) {
                console.error('Errore durante il recupero dei dati:', error);
            });
    }

    $scope.loadDiffidati = function () {


        $http.get('http://localhost:3000/diffidati')
            .then(function (response) {

                console.log('Dati recuperati con successo:', response.data);

                $scope.datiTabella = response.data;
                $scope.totalPages = Math.ceil($scope.datiTabella.length / $scope.limitRow);
            })
            .catch(function (error) {
                console.error('Errore durante il recupero dei dati:', error);
            });
    }

    $scope.loadDiffidatiScaduti = function () {


        $http.get('http://localhost:3000/diffidatiScaduti')
            .then(function (response) {

                console.log('Dati recuperati con successo:', response.data);

                $scope.datiTabella = response.data;
                $scope.totalPages = Math.ceil($scope.datiTabella.length / $scope.limitRow);
            })
            .catch(function (error) {
                console.error('Errore durante il recupero dei dati:', error);
            });
    }

    $scope.setArchiviato = function (id) {


        Swal.fire({
            title: "Scegli la motivazione",
            input: "select",
            inputOptions: {
                "deceduto": "Deceduto",
                "no_rinnovo": "Non vuole rinnovare",
                "altro": "altro"
            },
            inputPlaceholder: "Seleziona",
            showCancelButton: true,
            inputValidator: (value) => {
                return new Promise((resolve) => {


                    $http.get('http://localhost:3000/setArchiviato?id=' + id + '&motivo=' + value).then(function (response) {

                        console.log('Dati recuperati con successo:', response.data);

                        Swal.fire({
                            title: 'Dati salvati correttamente!',
                            text: ' ',
                            icon: 'success',
                            showConfirmButton: false,
                            timer: 2000
                        })


                    })
                        .catch(function (error) {
                            console.error('Errore durante il recupero dei dati:', error);
                        });

                });
            }
        });





    }


    $scope.loadAll = function () {


        $http.get('http://localhost:3000/utenti')
            .then(function (response) {

                console.log('Dati recuperati con successo:', response.data);

                $scope.datiTabella = response.data;
                $scope.totalPages = Math.ceil($scope.datiTabella.length / $scope.limitRow);


            })
            .catch(function (error) {
                console.error('Errore durante il recupero dei dati:', error);
            });
    }

    $scope.currentPage = 1;
    $scope.limitRow = 10;


    $scope.goToPage = function (pageNumber) {
        $scope.currentPage = pageNumber;

    };


    $scope.range = function (size) {
        return new Array(size).fill(0).map((_, i) => i);
    };


    $scope.$watch('limitRow', function (newLimitRow, oldLimitRow) {
        console.log("Limit Row changed. New value:", newLimitRow);
        if (newLimitRow !== oldLimitRow) {

            console.log("Total pages updated. New value:", $scope.totalPages);

            // Aggiorna la pagina corrente se necessario
            if ($scope.currentPage > $scope.totalPages) {
                $scope.currentPage = $scope.totalPages;
            }
        }
    });


    $scope.goToPage = function (pageNumber) {
        if (pageNumber >= 1 && pageNumber <= $scope.totalPages) {
            $scope.currentPage = pageNumber;
        }
    };

    $scope.updateLimitRow = function (val) {

        $scope.limitRow = parseInt(val);
        $scope.totalPages = Math.ceil($scope.datiTabella.length / $scope.limitRow);
    };

});