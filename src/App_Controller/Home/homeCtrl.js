﻿app.controller('homeCtrl', function ($scope, $rootScope,$http, $log, $routeParams, $timeout) {



    $scope.userData = {};
    $scope.tore = '✅';
    $scope.dati = {};

    $scope.submitForm = function () {


        // Effettua una richiesta POST al server Express
        $http.post('http://localhost:3000/insert', $scope.userData)
            .then(function (response) {
                console.log('Dati inseriti con successo:', response.data);
                // Pulisci il form dopo l'inserimento dei dati
                $scope.userData = {};
            })
            .catch(function (error) {
                console.error('Errore durante l\'invio dei dati al server:', error);
            });
    };


    $scope.load = function () {
        $http.get('http://localhost:3000/utenti')
            .then(function (response) {
                console.log('Dati recuperati con successo:', response.data);
                $scope.dati = response.data;
            })
            .catch(function (error) {
                console.error('Errore durante il recupero dei dati:', error);
            });
    }
    $scope.loadID = function (id) {
        $http.get('http://localhost:3000/utente?id=' + id)
            .then(function (response) {
                console.log('Dati recuperati con successo:', response.data);
                debugger
                $scope.dati = [response.data];
            })
            .catch(function (error) {
                console.error('Errore durante il recupero dei dati:', error);
            });
    }

});