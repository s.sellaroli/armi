app.directive("datapicker", function ($timeout, $parse, $rootScope) {
    return {
        restrict: 'AC',
        require: 'ngModel',
        scope: { data: "=ngModel" },
        link: function (scope, element, attrs, ngModel) {

            try {

                // plugin usato : https://www.daterangepicker.com/

                $timeout(function () {

                    let format = "DD/MM/YYYY"
                    let time = false;
                    if (!isEmpty(attrs.format)) {
                        format = attrs.format;
                        time = attrs.format.includes("HH")
                    }


                    let dat = $(element).val();


                    if (!isEmpty(dat)) {

                        if (!time) {
                            if (!moment(dat, 'DD/MM/YYYY', true).isValid()) {
                                $(element).val(gestioneDate.dataToITA(dat, time));
                            }
                        }
                        else {

                            if (!moment(dat, 'DD/MM/YYYY HH:mm', true).isValid()) {

                                $(element).val(gestioneDate.dataToITA(dat, time));
                            }
                        }


                    }


                    let option = {
                        singleDatePicker: true,
                        showDropdowns: true,
                        autoApply: false,
                        autoUpdateInput: true,
                        drops: "auto",
                        locale: {
                            "format": format,
                            cancelLabel: 'Canella',
                            applyLabel: 'Applica',
                            "separator": " - ",

                            "fromLabel": 'Da',
                            "toLabel": 'A',
                            "customRangeLabel": 'Personalizza',
                            "daysOfWeek": [
                                'Dom',
                                'Lun',
                                'Mar',
                                'Mer',
                                'Gio',
                                'Ven',
                                'Sab',
                            ],
                            "monthNames": [
                                'Gennaio',
                                'Febbraio',
                                'Marzo',
                                'Aprile',
                                'Maggio',
                                'Giugno',
                                'Luglio',
                                'Agosto',
                                'Settembre',
                                'Ottobre',
                                'Novembre',
                                'Dicembre',
                            ],
                            "firstDay": 0

                        }
                    }


                    if (!isEmpty(attrs.parent)) {
                        option.parentEl = attrs.parent;
                    }

                    if (!isEmpty(attrs.fromtoday)) {

                        option.minDate = moment().add(1, 'days').format("DD/MM/YYYY")
                        if (attrs.compreso = 'true') {
                            option.minDate = moment().format("DD/MM/YYYY HH:mm")
                        }
                    }

                    if (!isEmpty(attrs.fromrichiami)) {

                        if (!isEmpty(dat)) {

                            option.minDate = moment().format("DD/MM/YYYY HH:mm");

                        }

                    }

                    if (attrs.datafine == 'true') {

                        let a = moment().format("DD/MM/YYYY HH:mm")
                        dat = a.slice(0, -5) + "23:59";

                        attrs.startDate = dat;
                    }

                    if (!isEmpty(attrs.format) && attrs.format.toString().toLowerCase().includes("hh:mm")) {

                        option.format = format;
                        option.timePicker = true;
                        option.timePicker24Hour = true;
                        option.timePickerIncrement = 1;

                    }
                    
                    if (attrs.multiple == true) {

                        option.singleDatePicker = false;

                    }



                    let empty = isEmpty($(element).val());

                    $(element).daterangepicker(option, function (start, end) {


                        if (attrs.multiple == true) {
                            $(element).data('daterangepicker').setStartDate(start).trigger('change');
                            $(element).data('daterangepicker').setEndDate(end).trigger('change');

                            start = moment(start).format(format);
                            end = moment(end).format(format);

                            ngModel.$setViewValue(start + ' - ' + end);
                        }
                        else {

                            start = moment(start).format(format);
                            $(element).val(start).trigger('change');
                            ngModel.$setViewValue(start);
                        }

                    });

                    if (empty) {
                        $(element).val('').trigger('change');
                    }

                    $(element).on('cancel.daterangepicker', function (ev, picker) {
                        $(element).val('').trigger('change');
                    });



                    let old = dat;

                    scope.$watch('data',
                        function (newValue) {
                            if (!isEmpty(newValue)) {
                                if (attrs.multiple != true && attrs.multiple == false && !isEmpty(attrs.multiple)) {

                                    $timeout(function () {
                                        if (newValue != undefined && newValue != old) {
                                            old = newValue


                                            $(element).data('daterangepicker').setStartDate(newValue);
                                            $(element).data('daterangepicker').setEndDate(newValue);
                                            ngModel.$setViewValue(newValue);



                                        }
                                        else {
                                            let val = $(element).val();
                                            if (val != old) {
                                                old = val;

                                                $(element).data('daterangepicker').setStartDate(val);
                                                $(element).data('daterangepicker').setEndDate(val);
                                                ngModel.$setViewValue(val);


                                            }
                                        }
                                    }, 1)



                                }

                            }


                        }
                        ,
                        true);

                });
            } catch (error) {
                console.error(error);
            }


        }
    };
});

