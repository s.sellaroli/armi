﻿

app.controller('mainCtrl', function ($rootScope, $log, $window, $location, $timeout, $interval, $sce) {

    $rootScope.go = function (path = '/') {
        try {
            $log.debug(`Richiesta cambio pagina: ${path}`)
            $location.hash('');
            if (path[0] != '/') {
                path = '/' + path.toLowerCase();
            }
            if (path.includes("/errore/") && $rootScope.sessione.isLogged()) {
                path.replace("errore", "errorei");
            }

            $location.path(path);

        }
        catch (ex) {
            $log.log(ex);
        }
    };
    $rootScope.goBack = function () {
        $window.history.back();
    };
    $rootScope.isEmpty = function (val) {
        return isEmpty(val);
    };


    $rootScope.trustHtml = function (html) {
        try {

            return $sce.trustAsHtml(html);
        } catch (error) {
            $log.debug(error);
        }

    };

    $rootScope.formatDate = function (input) {

        if (isEmpty(input) || input == "NULL") {
            return ""
        }

        if (!input) return input;

        var parts = input.split('-');
        if (parts.length !== 3) return input;

        var year = parts[0];
        var month = parts[1];
        var day = parts[2];

        return day + '/' + month + '/' + year;
    }

    $rootScope.tipoTitolo = function (input) {

        let text = '';

        switch (input) {
            case 1:
            case '1':
                text = 'Detenzione';
                break;
            case 2:
            case '2':
                text = 'Tiro a Volo';
                break;
            case 3:
            case '3':
                text = 'Uso Caccia';
                break;
            case 4:
            case '4':
                text = 'Difesa Personale';
                break;
            case 5:
            case '5':
                text = 'GPG';
                break;
                ;
        }

        return text;

    }
});  