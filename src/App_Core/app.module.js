﻿var app = angular.module('app',
    [
        'ng',
        'ngRoute',
        'ngSanitize',
    ]);


app.run(function ($rootScope,$http) {
  
    $rootScope.vers = "1.0.7"
    
    $rootScope.update = false;

    var darkMode = setInterval(function () {
        if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
            document.getElementsByTagName("html")[0].setAttribute("data-theme", "dark");

        }
    }, 1);

    $rootScope.$on('$routeChangeStart', async function (event, current, previous) {

       
    });

    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {

    });


    $rootScope.$on('$viewContentLoaded', function (current, event, next) {


    });
 

});

