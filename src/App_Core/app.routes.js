﻿
app.config(function ($routeProvider, $locationProvider, $logProvider) {


    //$qProvider.errorOnUnhandledRejections(false);
    $logProvider.debugEnabled(false);
    if (localStorage["forceDebug"] == 'true') {
        $logProvider.debugEnabled(true);
    }


    $routeProvider.caseInsensitiveMatch = true;
    $routeProvider
        .when('/', {
            templateUrl: 'App_Controller/Home/home.html',
            pagina: "Home",

        })
        .when('/porto', {
            templateUrl: 'App_Controller/Porto/porto.html',
            pagina: "Porto D'Armi",

        })
        .when('/portoEdit/:id', {
            templateUrl: 'App_Controller/Porto/portoEdit.html',
            pagina: "Porto D'Armi",

        })
        .when('/statistiche', {
            templateUrl: 'App_Controller/Statistiche/statistiche.html',
            pagina: "Statistiche",

        })
        .when('/details/:type?', {
            templateUrl: 'App_Controller/Details/details.html',
            pagina: "Details",

        })
        //======================================= SE NON TROVA LA PAGINA ===================================================
        .otherwise({ redirectTo: '/404' });

    // $locationProvider.html5Mode({
    //     enabled: true,
    //     requireBase: true,
    //     hashPrefix: '*',
    //     rewriteLinks: true
    // });

});